<?php

require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$id = (int)$matches[1];

$conn = Db::GetNewConnection();
$result = Db::ExecuteFirst("SELECT * FROM adcopies WHERE id = '$id'", $conn);
Db::CloseConnection($conn);

$linknum = str_pad($id, 3, "0", STR_PAD_LEFT);

$link = "http://spectrumautomation.com/adcopy_pdf/" . $linknum .".pdf";


//$renderpage = "<h2>".$result['title']." </h2>";
$renderpage = "<div style='width:100%; text-align:right;'>(<a href='".$link."' target='_blank'>Downloadable PDF</a>)</div>";
$renderpage .= $result['body'];

$context['title'] = $result['title'];
//$context['body'] = $result['body'];
$context['body'] = $renderpage;

echo $twig->render('adcopy.html', $context);