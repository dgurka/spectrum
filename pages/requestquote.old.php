<?php

require_once(BASE_DIR . "bootstrap.php");


function sterilize($input, $is_sql = false){
   
   /*
   +-------------------------------------------------------------------+
   |______________________The_Sterilizer_Function______________________|
   | PHP 5+ ONLY - Used to prevent SQLI and XSS attacks via user input |
   |                                                                   |
   | 1 *REQUIRED* value, 1 <OPTIONAL> value to call this function:     |
   |   $input  = User input string to be cleansed                      |
   |   #is_sql = Boolean. Whether or not $input is a sql query         |
   +-------------------------------------------------------------------+
   | Example of use:                                                   |
   |   $username = sterilize($_POST['username']);                      |
   |   $query = "SELECT * FROM users WHERE username = '$username'";    |
   +-------------------------------------------------------------------+
   */
   
    $input = htmlentities($input, ENT_QUOTES);
 
    if(get_magic_quotes_gpc ())
    {
        $input = stripslashes ($input);
    }
 
    if ($is_sql)
    {
        $input = mysql_real_escape_string ($input);
    }
 
    $input = strip_tags($input);
    $input = str_replace("
", "\n", $input);
 
    return $input;
}

$context = getDefaultContext();


if($_SERVER["REQUEST_METHOD"] == "POST"){

  $_headers = array(
    "From: sales@spectrumautomation.com",
    "Bcc: frank@falsealarmsoftware.com,frank@wsbtm.com,dgurka@enablepoint.com,enablepoint@gmail.com, admin@spectrumautomation.com",
    "Content-Type: text/html"
  );
  $h = implode("\r\n", $_headers);
	$toEmail = "sales@spectrumautomation.com";
	
	$subject = "REQUEST QUOTE - SPECTRUM";

$body = "<table width=\"100%\" border=\"0\" cellspacing=\"3\" cellpadding=\"0\" style=\"/* [disabled]margin:5px; */ border-bottom: solid 4px black;\">
  <tr>
    <td colspan=\"2\" rowspan=\"8\"><p><img src=\"http://spectrumautomation.com/templates/spectrum/images/logo.png\" class=\"logo\" alt=\"Spectrum\"></p>
	<p>34447 Schoolcraft &#8226; Livonia, Michigan 48150<br />
        Phone: (734) 522-2160 &#8226; Fax: (734) 522-4671<br />
        WWW.SpectrumAutomation.com<br />
      Email: Sales@SpectrumAutomation.com</p></td>
    <td colspan=\"2\" rowspan=\"2\">&nbsp;</td>
    <td width=\"94\">Date:</td>
    <td width=\"220\">".sterilize($_POST['date'])."</td>
  </tr>
  <tr>
    <td>Due Date:</td>
    <td>".sterilize($_POST['due-date'])."</td>
  </tr>
  <tr>
    <td width=\"79\">Agency:</td>
    <td width=\"288\">".sterilize($_POST['agency'])."</td>
    <td>Will Deliver</td>
    <td>";
	if(!empty($_POST['will-deliver'])== "yes"){ $body .= "YES"; } else { $body .= "NO"; }
	$body .= "</td>
  </tr>
  <tr>
    <td>Salesman:</td>
    <td>".sterilize($_POST['salesman'])."</td>
    <td>Mail Direct</td>
    <td>";
	if(!empty($_POST['mail-direct'])== "yes"){ $body .= "YES"; } else { $body .= "NO"; }
	$body .= "</td>
  </tr>
  <tr>
    <td colspan=\"2\">&nbsp;</td>
    <td>Fax</td>
    <td>";
	if(!empty($_POST['inquiryby-fax'])== "yes"){ $body .= "YES"; } else { $body .= "NO"; }
	$body .= "</td>
    </tr>
  <tr>
    <td colspan=\"2\">&nbsp;</td>
    <td>Email</td>
    <td>";
	if(!empty($_POST['inquiryby-email'])== "yes"){ $body .= "YES"; } else { $body .= "NO"; }
	$body .= "</td>
  </tr>
  <tr>
    <td colspan=\"2\">&nbsp;</td>
    <td>Web</td>
    <td>";
	if(!empty($_POST['inquiryby-web'])== "yes"){ $body .= "YES"; } else { $body .= "NO"; }
	$body .= "</td>
  </tr>
  <tr>
    <td colspan=\"2\">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>";
		
	$body .="<table width=\"100%\" style=\"margin:5px;border-bottom: solid 4px black;\">
  <tbody>
    <tr>
      <td colspan=\"2\">Customer:</td>
      <td colspan=\"2\">".sterilize($_POST['required-Name'])."</td>
      <td width=\"9%\">Attention:</td>
      <td width=\"43%\">".sterilize($_POST['attention'])."</td>
    </tr>
    <tr>
      <td colspan=\"2\">Company Name:</td>
      <td colspan=\"2\">".sterilize($_POST['Company-Name'])."</td>
      <td rowspan=\"2\">Request By: </td>
      <td rowspan=\"2\">".sterilize($_POST['request-by'])."</td>
    </tr>
    <tr>
      <td colspan=\"2\">Div. Of:</td>
      <td colspan=\"2\">".sterilize($_POST['division'])."</td>
    </tr>
    <tr>
      <td colspan=\"2\">Address:</td>
      <td colspan=\"2\">".sterilize($_POST['Address'])."</td>
      <td>Inquiry #:</td>
      <td>".sterilize($_POST['Inquiry-#'])."</td>
    </tr>
    <tr>
      <td colspan=\"2\">City, State:</td>
      <td colspan=\"2\">".sterilize($_POST['City']).", ".sterilize($_POST['state'])."</td>
      <td>User:</td>
      <td>".sterilize($_POST['User'])."</td>
    </tr>
    <tr>
      <td width=\"4%\">Zip:</td>
      <td width=\"10%\">".sterilize($_POST['Zip-code'])."</td>
      <td width=\"6%\">Phone:</td>
      <td width=\"28%\">(".sterilize($_POST['phone-p1']).") ".sterilize($_POST['phone-p2'])."-".sterilize($_POST['phone-p3'])."</td>
      <td>Fax</td>
      <td>(".sterilize($_POST['fax-p1']).") ".sterilize($_POST['fax-p2'])."-".sterilize($_POST['fax-p3'])."</td>
    </tr>
    <tr>
      <td>Email: </td>
      <td colspan=\"5\">".sterilize($_POST['required-email'])."</td>
    </tr>
  </tbody>
</table>";
	
	$body .= "<table width=\"100%\"><tr><td width=\"50%\" valign=\"top\"><table width=\"100%\" style=\"margin:5px;border-bottom: solid 4px black;\">";
	$body .= "<tr><td><strong>Specifications:</strong><br />Number of Machines Required:</td><td>".sterilize($_POST['machines-required']);
		if(!empty($_POST['del-req'])== "yes"){ $body .= "&nbsp;Del. Req."; }
	$body .="</td></tr>";
	$body .= "<tr><td>Storage:</td><td>".sterilize($_POST['Storage'])." pieces</td></tr>";
	$body .= "<tr><td>&nbsp;</td><td>".sterilize($_POST['cuft'])." cu. ft.</td></tr>";
	$body .= "<tr><td>&nbsp;</td><td>".sterilize($_POST['hours'])." hours</td></tr>";
	$body .= "<tr><td>&nbsp;</td><td>".sterilize($_POST['sec-cycle-total'])." sec/cycle (total)</td></tr>";
	$body .= "<tr><td>&nbsp;</td><td>".sterilize($_POST['sec-cycle-load-time'])." sec/cycle load time</td></tr>";
	$body .= "<tr><td>Production Rate</td><td>".sterilize($_POST['actual-total'])." pieces/minute</td></tr>";
	$body .= "<tr><td>&nbsp;</td><td>".sterilize($_POST['pieces-hour'])." pieces/hour</td></tr>";
	
	$body .= "<tr><td>&nbsp;</td><td>";
	
	if(!empty($_POST['C1'])== "yes"){ $body .= "YES"; }
	
	$body .= " bulk feed only</td></tr>";
	$body .= "<tr><td colspan=\"2\">".sterilize($_POST['Number-of-paths-on'])." paths on ".sterilize($_POST['Number-of-paths-centers'])." centers</td></tr>";
	$body .= "<tr><td>Part Discharge Height</td><td>".sterilize($_POST['part-discharge-height'])."</td></tr>";
	$body .= "</table><table width='100%' style='margin:5px;'>";
	$body .= "<tr><td><b>Orientation:</b></td></tr>"; 
	
	if(!empty($_POST['end-to-end'])== "yes"){ $body .= "<tr><td>End to End</td></tr>"; }
	if(!empty($_POST['Diameter-to-diameter'])== "yes"){ $body .= "<tr><td>Diameter to diameter</td></tr>"; }
	if(!empty($_POST['side-to-side'])== "yes"){ $body .= "<tr><td>Side to side</td></tr>"; }
	if(!empty($_POST['rolling'])== "yes"){ $body .= "<tr><td>Rolling</td></tr>"; }
	if(!empty($_POST['sliding'])== "yes"){ $body .= "<tr><td>Sliding</td></tr>"; }
	if(!empty($_POST['axis'])== "yes"){ $body .= "<tr><td>Axis ".sterilize($_POST['deg-from-horizontal'])." Deg. From Horizontal</td></tr>"; }
	if(!empty($_POST['axis-horizontal'])== "yes"){ $body .= "<tr><td>Axis Horizontal</td></tr>"; }
	if(!empty($_POST['axis-vertical'])== "yes"){ $body .= "<tr><td>Axis Vertical</td></tr>"; }
	if(!empty($_POST['radial'])== "yes") {"<tr><td>Radial</td></tr>"; }
	if(!empty($_POST['face-selected'])== "yes"){ $body .= "<tr><td>Face selected</td></tr>"; }
	if(!empty($_POST['end-selected'])== "yes"){ $body .= "<tr><td>End Selected</td></tr>"; } 
	
	$body .= "<tr><td><br/>Operation Description:<br/><br/>".sterilize($_POST['Describe-Operation-being-fed'])."</td></tr>";
	
	if(!empty($_POST['customer-specs'])== "yes"){ $body .= "<tr><td>Customer Specs. (Provide Copy)</td></tr>"; } 
	if(!empty($_POST['drawings'])== "yes"){ $body .= "<tr><td>Drawings - Customer Provided Paper</td></tr>"; } 
	if(!empty($_POST['similar-adcopy'])== "yes"){ $body .= "<tr><td>Similar to ad copy #".sterilize($_POST['similar-adcopynum'])."</td></tr>"; } 
	if(!empty($_POST['similar-specnum'])== "yes"){ $body .= "<tr><td>Similar to Spectrum Ser. #".sterilize($_POST['similar-specsernum'])."</td></tr>"; } 

	
	
	$body .= "</table></td><td width='50%' valign='top'>";
	$body .= "<table width='100%' style='margin:5px; border-bottom:solid 2px black;'>";
	$body .= "<tr><td><b>Part:</b></td><td>&nbsp;</td></tr>";
	$body .= "<tr><td>Name:</td><td>".sterilize($_POST['Part-name'])."</td></tr>";
	$body .= "<tr><td>Number:</td><td>".sterilize($_POST['Part-number'])."</td></tr>";
	$body .= "<tr><td>Prints:</td><td>".sterilize($_POST['Part-prints'])."</td></tr>";
	$body .= "<tr><td>Samples:</td><td>".sterilize($_POST['Part-samples'])."</td></tr>";
	$body .= "<tr><td>Return:</td><td>".sterilize($_POST['Part-return'])."</td></tr>";
	
	$body .= "</table><table width='100%' style='margin:5px; border-bottom:solid 4px black;'>";
  	$body .= "<tr><td><strong>Operation recieved from:</strong><br /><b>Condition:</b></td></tr>";
	
	if(!empty($_POST['dry'])== "yes"){ $body .= "<tr><td>Dry</td></tr>"; } 
	if(!empty($_POST['wet'])== "yes"){ $body .= "<tr><td>Wet</td></tr>"; } 
	if(!empty($_POST['oil-scum'])== "yes"){ $body .= "<tr><td>Oil Scum</td></tr>"; } 
	if(!empty($_POST['oil-dripping'])== "yes"){ $body .= "<tr><td>Oil Dripping</td></tr>"; } 
	if(!empty($_POST['soft'])== "yes"){ $body .= "<tr><td>Soft</td></tr>"; } 
	if(!empty($_POST['hard'])== "yes"){ $body .= "<tr><td>Hard</td></tr>"; } 
	if(!empty($_POST['magnetized'])== "yes"){ $body .= "<tr><td>Magnetized</td></tr>"; } 
	if(!empty($_POST['clean'])== "yes"){ $body .= "<tr><td>Clean</td></tr>"; } 
	if(!empty($_POST['dirty'])== "yes"){ $body .= "<tr><td>Dirty</td></tr>"; } 
	if(!empty($_POST['delicate'])== "yes"){ $body .= "<tr><td>Delicate</td></tr>"; } 
	if(!empty($_POST['chips'])== "yes"){ $body .= "<tr><td>Chips</td></tr>"; } 
	if(!empty($_POST['burrs'])== "yes"){ $body .= "<tr><td>Burrs</td></tr>"; } 
	if(!empty($_POST['tramp'])== "yes"){ $body .= "<tr><td>Tramp</td></tr>"; } 
	if(!empty($_POST['mixed-parts'])== "yes"){ $body .= "<tr><td>Mixed Parts</td></tr>"; } 
	if(!empty($_POST['special-coatings'])== "yes"){ $body .= "<tr><td>Special Coatings</td></tr>"; } 
	
	$body .= "<tr><td><br/>Parts Description:<br/><br/>".sterilize($_POST['Describe-Parts'])."</td></tr>";
	$body .= "</table><table width='100%'>";
	$body .= "<tr><td><b>Needs:</b></td></tr>";
	
	if(!empty($_POST['Needs-track'])== "yes"){ $body .= "<tr><td>Track</td></tr>"; } 
	if(!empty($_POST['Needs-track-switch'])== "yes"){ $body .= "<tr><td>Track Switch</td></tr>"; } 
	if(!empty($_POST['Needs-clean-out-door'])== "yes"){ $body .= "<tr><td>Clean out door</td></tr>"; } 
	if(!empty($_POST['Needs-escapement'])== "yes"){ $body .= "<tr><td>Escapement</td></tr>"; } 
	if(!empty($_POST['Needs-bin-elevator-covers'])== "yes"){ $body .= "<tr><td>Bin & Elevator Covers</td></tr>"; } 
	if(!empty($_POST['Needs-oil-drip-pan'])== "yes"){ $body .= "<tr><td>Oil Drip Pan</td></tr>"; } 
	if(!empty($_POST['Needs-wire-to-j-box'])== "yes"){ $body .= "<tr><td>Wire to j-box</td></tr>"; } 
	if(!empty($_POST['Needs-gentle-handling'])== "yes"){ $body .= "<tr><td>Gentle Handling</td></tr>"; } 

	if(!empty($_POST['Needs-electrical-controls'])== "yes"){ $body .= "<tr><td>Electrical Controls</td></tr>"; } 
	if(!empty($_POST['Needs-JIC'])== "yes"){ $body .= "<tr><td>JIC</td></tr>"; } 
	if(!empty($_POST['Needs-noise-control'])== "yes"){ $body .= "<tr><td>Noise Control ".sterilize($_POST['dBA']) ." dBA @ ".sterilize($_POST['feet']) ."</td></tr>"; } 
	if(!empty($_POST['Needs-casters-floor-locks'])== "yes"){ $body .= "<tr><td>Casters-floor locks</td></tr>"; } 
	if(!empty($_POST['Needs-conveyor'])== "yes"){ $body .= "<tr><td>Conveyor</td></tr>"; } 
	if(!empty($_POST['Needs-inspect-sort'])== "yes"){ $body .= "<tr><td>Inspect Sort</td></tr>"; } 
	if(!empty($_POST['Needs-Gon-support-stand'])== "yes"){ $body .= "<tr><td>Gon Support Stand</td></tr><tr><td>Size of Gon: ".sterilize($_POST['Size-of-Gon']) ."</td></tr><tr><td>Max wt of Gon: ".sterilize($_POST['max-wt-of-Gon']) ."</td></tr>"; } 
	if(!empty($_POST['Needs-Gon-dumper'])== "yes"){ $body .= "<tr><td>Gon Dumper</td></tr>"; } 
	if(!empty($_POST['Needs-load-place'])== "yes"){ $body .= "<tr><td>Load Place</td></tr>"; } 
	if(!empty($_POST['Needs-weigh-and-dump'])== "yes"){ $body .= "<tr><td>Weigh and Dump:</td></tr><tr><td>Dumps/hour: ".sterilize($_POST['Needs-dumps-hour']) ."</td></tr><tr><td>lbs/dump: ".sterilize($_POST['Needs-lbs-dump']) ."</td></tr><tr><td>lbs/hour: ".sterilize($_POST['Needs-lbs-hour']) ."</td></tr>"; } 
	 $body .= "</table></td></tr></table>";
	
	/* Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. */
	if (mail($toEmail,$subject,$body,$h)) {
	  $context["message"] = "<div style='margin-bottom:25px; font-weight:bold'>Thank you for submitting your quote to Spectrum Automation. We will review your quote and contact you soon. </div>";
	} else {
	  $context["message"] = "<b>We apologize but your quote was unable to be submitted. Please try again.</b>";
	}

}

echo $twig->render('requestquote.html', $context);