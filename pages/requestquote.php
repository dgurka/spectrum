<?php

require_once(BASE_DIR . "bootstrap.php");


function sterilize($input, $is_sql = false){
   
   /*
   +-------------------------------------------------------------------+
   |______________________The_Sterilizer_Function______________________|
   | PHP 5+ ONLY - Used to prevent SQLI and XSS attacks via user input |
   |                                                                   |
   | 1 *REQUIRED* value, 1 <OPTIONAL> value to call this function:     |
   |   $input  = User input string to be cleansed                      |
   |   #is_sql = Boolean. Whether or not $input is a sql query         |
   +-------------------------------------------------------------------+
   | Example of use:                                                   |
   |   $username = sterilize($_POST['username']);                      |
   |   $query = "SELECT * FROM users WHERE username = '$username'";    |
   +-------------------------------------------------------------------+
   */
   
    $input = htmlentities($input, ENT_QUOTES);
 
    if(get_magic_quotes_gpc ())
    {
        $input = stripslashes ($input);
    }
 
    if ($is_sql)
    {
        $input = mysql_real_escape_string ($input);
    }
 
    $input = strip_tags($input);
    $input = str_replace("
", "\n", $input);
 
    return $input;
}

$context = getDefaultContext();


if($_SERVER["REQUEST_METHOD"] == "POST"){

  $_headers = array(
    "From: sales@spectrumautomation.com",
    "Bcc: frank@falsealarmsoftware.com,frank@wsbtm.com,dgurka@enablepoint.com,enablepoint@gmail.com, admin@spectrumautomation.com",
    "Content-Type: text/html"
  );
  $h = implode("\r\n", $_headers);
	$toEmail = "sales@spectrumautomation.com";
	
	$subject = "REQUEST QUOTE - SPECTRUM";

$body ="";
$body .="<table width=\"100%\" border=\"0\" cellspacing=\"3\" cellpadding=\"0\" style=\"/* [disabled]margin:5px; */ border-bottom: solid 4px black;\">
  <tr>
    <td colspan=\"2\" rowspan=\"6\"><p><img src=\"http://spectrumautomation.com/templates/spectrum/images/logo.png\" class=\"logo\" alt=\"Spectrum\"></p>
      <p>34447 Schoolcraft &#8226; Livonia, Michigan 48150<br />
        Phone: (734) 522-2160 &#8226; Fax: (734) 522-4671<br />
        WWW.SpectrumAutomation.com<br />
        Email: Sales@SpectrumAutomation.com</p></td>
    <td colspan=\"2\" rowspan=\"2\">&nbsp;</td>
    <td width=\"151\">Date:</td>
    <td width=\"163\" style=\"border-bottom: 1px solid black !important;\">".sterilize($_POST['date'])."</td>
  </tr>
  <tr>
    <td>Quote Due Date:</td>
    <td style=\"border-bottom: 1px solid black !important;\">".sterilize($_POST['due-date'])."</td>
  </tr>
  <tr>
    <td width=\"60\">Agency:</td>
    <td width=\"288\" style=\"border-bottom: 1px solid black !important;\">".sterilize($_POST['agency'])."</td>
    <td>Will Deliver</td>
    <td>";
      if(!empty($_POST['will-deliver'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
      $body .= "</td>
  </tr>
  <tr>
    <td>Salesman:</td>
    <td style=\"border-bottom: 1px solid black !important;\">".sterilize($_POST['salesman'])."</td>
    <td>Mail Direct</td>
    <td>";
      if(!empty($_POST['mail-direct'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
      $body .= "</td>
  </tr>
  <tr>
    <td colspan=\"2\">&nbsp;</td>
    <td>Fax</td>
    <td>";
      if(!empty($_POST['inquiryby-fax'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
      $body .= "</td>
  </tr>
  <tr>
    <td colspan=\"4\">&nbsp;</td>
  </tr>
</table>
<table width=\"100%\" style=\"margin:5px;border-bottom: solid 4px black;\">
  <tbody>
    <tr>
      <td colspan=\"2\">CUSTOMER:</td>
      <td colspan=\"2\">".sterilize($_POST['required-Name'])."</td>
      <td width=\"12%\">ATTENTION: </td>
      <td width=\"18%\">".sterilize($_POST['attention'])."</td>
      <td colspan=\"2\">Inquiry by:</td>
    </tr>
    <tr>
      <td colspan=\"2\">DIV. OF:</td>
      <td colspan=\"2\">".sterilize($_POST['division'])."</td>
      <td>REQUEST BY:</td>
      <td>".sterilize($_POST['request-by'])."</td>
      <td width=\"7%\">Phone </td>
      <td width=\"11%\">";
        if(!empty($_POST['inquiry-phone'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
        $body .="</td>
    </tr>
    <tr>
      <td colspan=\"2\">ADDRESS:</td>
      <td colspan=\"2\">".sterilize($_POST['Address'])."</td>
      <td>INQUIRY #:</td>
      <td>".sterilize($_POST['Inquiry-#'])."</td>
      <td>Mail</td>
      <td>";
        if(!empty($_POST['inquiry-mail'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
        $body .="</td>
    </tr>
    <tr>
      <td colspan=\"2\">CITY, STATE:</td>
      <td colspan=\"2\">".sterilize($_POST['City']).", ".sterilize($_POST['state'])."</td>
      <td>USER:</td>
      <td>".sterilize($_POST['User'])."</td>
      <td>Rep</td>
      <td>";
        if(!empty($_POST['inquiry-rep'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
        $body .="</td>
    </tr>
    <tr>
      <td width=\"4%\">ZIP:</td>
      <td width=\"15%\">".sterilize($_POST['Zip-code'])."</td>
      <td width=\"7%\">PHONE: </td>
      <td width=\"26%\">(".sterilize($_POST['phone-p1']).") ".sterilize($_POST['phone-p2'])."-".sterilize($_POST['phone-p3'])."</td>
      <td>FAX:</td>
      <td>(".sterilize($_POST['fax-p1']).") ".sterilize($_POST['fax-p2'])."-".sterilize($_POST['fax-p3'])."</td>
      <td>Fax</td>
      <td>";
        if(!empty($_POST['inquiry-fax'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
        $body .="</td>
    </tr>
    <tr>
      <td>Email:</td>
      <td>".sterilize($_POST['required-email'])."</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>Email</td>
      <td>";
        if(!empty($_POST['inquiry-email'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
        $body .="</td>
    </tr>
  </tbody>
</table>

<table width=\"100%\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">
  <tr>
    <td width=\"50%\" align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" align=\"center\">
        <tbody>
          <tr>
            <td colspan=\"6\"><SPAN style=\"text-decoration:underline;\">SPECIFICATIONS:</SPAN></td>
          </tr>
          <tr>
            <td colspan=\"6\">Number of Machines Required: <span style=\"text-decoration:underline;\">".sterilize($_POST['machines-required'])."</span> Del. Req";
              if(!empty($_POST['del-req'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .="</td>
          </tr>
          <tr>
            <td width=\"35%\" rowspan=\"5\" valign=\"top\">Storage</td>
            <td colspan=\"5\">  <span style=\"text-decoration:underline;\">".sterilize($_POST['Storage'])."</span> Pieces</td>
          </tr>
          <tr>
            <td colspan=\"5\">  <span style=\"text-decoration:underline;\">".sterilize($_POST['hours'])."</span> Hours</td>
          </tr>
          <tr>
            <td colspan=\"5\">   <span style=\"text-decoration:underline;\">".sterilize($_POST['cuft'])."</span> Cu.Ft.</td>
          </tr>
          <tr>
            <td colspan=\"5\">  <span style=\"text-decoration:underline;\">".sterilize($_POST['sec-cycle-load-time'])."</span> Sec/Cycle               Load Time</td>
          </tr>
          <tr>
            <td colspan=\"5\">  <span style=\"text-decoration:underline;\">".sterilize($_POST['sec-cycle-total'])."</span> Sec/Cycle               (Total)</td>
          </tr>
          <tr>
            <td colspan=\"6\" valign=\"top\">Production Rate  
              (actual total)</td>
          </tr>
          <tr>
            <td valign=\"top\">  <span style=\"text-decoration:underline;\">".sterilize($_POST['actual-total'])."</span> Pieces/Minute</td>
            <td colspan=\"5\">  <span style=\"text-decoration:underline;\">".sterilize($_POST['pieces-hour'])."</span> Pieces/Hour</td>
          </tr>
          <tr>
            <td colspan=\"6\" valign=\"top\">";
              if(!empty($_POST['C1'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              
              $body .= "Bulk Feed Only</td>
          </tr>
          <tr>
            <td valign=\"top\">Number of Paths:</td>
            <td colspan=\"5\">  <span style=\"text-decoration:underline;\">".sterilize($_POST['Number-of-paths-on'])."</span> on  <span style=\"text-decoration:underline;\">".sterilize($_POST['Number-of-paths-centers'])."</span> &quot;               Centers</td>
          </tr>
          <tr>
            <td>Part               Discharge Height:</td>
            <td colspan=\"5\">  <span style=\"text-decoration:underline;\">".sterilize($_POST['part-discharge-height'])."</span></td>
          </tr>
          <tr>
            <td>Paint Color and Spec.:</td>
            <td colspan=\"5\">  <span style=\"text-decoration:underline;\">".sterilize($_POST['paint-color'])."</span></td>
          </tr>
          <tr>
            <td>Prime Color and Spec:</td>
            <td colspan=\"5\">  <span style=\"text-decoration:underline;\">".sterilize($_POST['prime-color'])."</span></td>
          </tr>
          <tr>
            <td colspan=\"6\">Orientation: (*)</td>
          </tr>
          <tr>
            <td colspan=\"2\">";
              if(!empty($_POST['Diameter-to-diameter'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "Diameter to diameter</td>
            <td width=\"47%\" colspan=\"4\" rowspan=\"11\">  </td>
          </tr>
          <tr>
            <td colspan=\"2\">";
              if(!empty($_POST['end-to-end'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "End to End</td>
          </tr>
          <tr>
            <td colspan=\"2\">";
              if(!empty($_POST['side-to-side'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "Side to Side</td>
          </tr>
          <tr>
            <td colspan=\"2\">";
              if(!empty($_POST['rolling'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "Rolling</td>
          </tr>
          <tr>
            <td colspan=\"2\">";
              if(!empty($_POST['sliding'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "Sliding</td>
          </tr>
          <tr>
            <td colspan=\"2\">";
              if(!empty($_POST['axis-horizontal'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "Axis Horizontal</td>
          </tr>
          <tr>
            <td colspan=\"2\">";
              if(!empty($_POST['axis-vertical'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "Axis Vertical</td>
          </tr>
          <tr>
            <td colspan=\"2\">";
              if(!empty($_POST['axis'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "Axis <span style=\"text-decoration:underline;\">".sterilize($_POST['deg-from-horizontal'])."</span> Deg. from Horizontal</td>
          </tr>
          <tr>
            <td colspan=\"2\">";
              if(!empty($_POST['face-selected'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "Face Selected (*)</td>
          </tr>
          <tr>
            <td colspan=\"2\">";
              if(!empty($_POST['end-selected'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "End Selected (*)</td>
          </tr>
          <tr>
            <td colspan=\"2\">";
              if(!empty($_POST['radial'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "Radially Controlled (*)</td>
          </tr>
          <tr>
            <td colspan=\"6\" valign=\"top\" style=\"margin:5px;border-top: solid 4px black;\">Describe operation being fed:  </td>
          </tr>
          <tr>
            <td colspan=\"6\" valign=\"top\"><span style=\"text-decoration:underline;\">".sterilize($_POST['Describe-Operation-being-fed'])."</span></td>
          </tr>
          <tr>
            <td colspan=\"6\" valign=\"top\">";
              if(!empty($_POST['customer-specs'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "Customer Specs. (Provide Copy)</td>
          </tr>
          <tr>
            <td colspan=\"6\" valign=\"top\">";
              if(!empty($_POST['drawings'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "Drawings - Customer Provided Paper</td>
          </tr>
          <tr>
            <td colspan=\"6\" valign=\"top\">";
              if(!empty($_POST['similar-adcopy'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "Similar to ad copy # <span style=\"text-decoration:underline;\">".sterilize($_POST['similar-adcopynum'])."</span></td>
          </tr>
          <tr>
            <td colspan=\"6\" valign=\"top\">";
              if(!empty($_POST['similar-specnum'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "Similar to Spectrum Ser. # <span style=\"text-decoration:underline;\">".sterilize($_POST['similar-specsernum'])."</span></td>
			  <tr>
              <td colspan=\"6\" valign=\"top\">&nbsp;</td>
            </tr>
            <tr>
              <td colspan=\"6\" valign=\"top\">Form #10634 - Quotation Request Form</td>
            </tr>
          </tr>
        </tbody>
      </table></td>
    <td width=\"50%\" align=\"left\" valign=\"top\"><table width=\"100%\" border=\"0\" align=\"center\" style=\"margin:5px;border-bottom: solid 4px black;\">
        <tbody>
          <tr>
            <td colspan=\"6\"><SPAN style=\"text-decoration:underline;\">PARTS</span></td>
          </tr>
          <tr>
            <td width=\"70\">Name</td>
            <td colspan=\"5\" style=\"border-bottom: 1px solid black !important;\">".sterilize($_POST['Part-name'])."</td>
          </tr>
          <tr>
            <td>Number</td>
            <td colspan=\"5\" style=\"border-bottom: 1px solid black !important;\">".sterilize($_POST['Part-number'])."</td>
          </tr>
          <tr>
            <td>Prints</td>
            <td colspan=\"5\" style=\"border-bottom: 1px solid black !important;\">".sterilize($_POST['Part-prints'])."</td>
          </tr>
          <tr>
            <td>Samples</td>
            <td colspan=\"5\" style=\"border-bottom: 1px solid black !important;\">".sterilize($_POST['Part-samples'])."</td>
          </tr>
          <tr>
            <td>Return</td>
            <td colspan=\"5\" >".sterilize($_POST['Part-return'])."</td>
          </tr>
        </tbody>
      </table>
      <table width=\"100%\" border=\"0\" align=\"center\" style=\"margin:5px;border-bottom: solid 4px black;\">
        <tbody>
          <tr>
            <td colspan=\"3\">OPERATION RECEIVED FROM: (*)</td>
          </tr>
          <tr>
            <td colspan=\"3\">CONDITION</td>
          </tr>
          <tr>
            <td>";
              if(!empty($_POST['dry'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "DRY</td>
            <td width=\"60%\">";
              if(!empty($_POST['clean'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "CLEAN</td>
          </tr>
          <tr>
            <td>";
              if(!empty($_POST['wet'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "WET</td>
            <td>";
              if(!empty($_POST['dirty'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "DIRTY</td>
          </tr>
          <tr>
            <td>";
              if(!empty($_POST['oil-scum'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "OIL SCUM</td>
            <td>";
              if(!empty($_POST['delicate'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "DELICATE</td>
          </tr>
          <tr>
            <td>";
              if(!empty($_POST['oil-dripping'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "OIL DRIPPING</td>
            <td>";
              if(!empty($_POST['burrs'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "BURRS</td>
          </tr>
          <tr>
            <td>";
              if(!empty($_POST['soft'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "SOFT</td>
            <td>";
              if(!empty($_POST['chips'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "CHIPS</td>
          </tr>
          <tr>
            <td>";
              if(!empty($_POST['hard'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "HARD</td>
            <td>";
              if(!empty($_POST['tramp'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "TRAMP (*)</td>
          </tr>
          <tr>
            <td>";
              if(!empty($_POST['magnetized'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "MAGNETIZED</td>
            <td>";
              if(!empty($_POST['mixed-parts'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "MIXED PARTS (*)</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>";
              if(!empty($_POST['special-coatings'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "SPECIAL COATINGS (*)</td>
          </tr>
        </tbody>
      </table>
      <table width=\"100%\" border=\"0\" align=\"center\">
        <tbody>
          <tr>
            <td colspan=\"5\"><SPAN style=\"text-decoration:underline;\">NEEDS</SPAN></td>
          </tr>
          <tr>
            <td>";
              if(!empty($_POST['Needs-track'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "TRACK</td>
            <td colspan=\"2\">";
              if(!empty($_POST['Needs-clean-out-door'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "CLEANOUT DOOR</td>
          </tr>
          <tr>
            <td>";
              if(!empty($_POST['Needs-track-switch'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "TRACK SWITCH</td>
            <td colspan=\"2\">";
              if(!empty($_POST['Needs-bin-elevator-covers'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "BIN &amp; ELEVATOR COVERS</td>
          </tr>
          <tr>
            <td>";
              if(!empty($_POST['Needs-escapement'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "ESCAPEMENT</td>
            <td colspan=\"2\">";
              if(!empty($_POST['Needs-oil-drip-pan'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "OIL DRIP PAN</td>
          </tr>
          <tr>
            <td>";
              if(!empty($_POST['Needs-wire-to-j-box'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "WIRE TO J-BOX</td>
            <td colspan=\"2\">";
              if(!empty($_POST['Needs-gentle-handling'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "GENTLE HANDLING</td>
          </tr>
          <tr>
            <td colspan=\"3\">";
              if(!empty($_POST['Needs-electrical-controls'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "ELECTRICAL CONTROLS</td>
          </tr>
          <tr>
            <td colspan=\"3\">";
              if(!empty($_POST['Needs-electrical-design'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "ELECTRICAL DESIGN ONLY</td>
          </tr>
          <tr>
            <td colspan=\"3\">";
              if(!empty($_POST['JIC'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "JIC</td>
          </tr>
          <tr>
            <td colspan=\"3\">";
              if(!empty($_POST['Needs-noise-control'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "NOISE CONTROL:  <span style=\"text-decoration:underline;\">".sterilize($_POST['dBA']) ."</span> dBA @ <span style=\"text-decoration:underline;\">".sterilize($_POST['feet']) ."</span> feet</td>
          </tr>
          <tr>
            <td colspan=\"3\">";
              if(!empty($_POST['Needs-casters-floor-locks'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "CASTERS-FLOOR LOCKS</td>
          </tr>
          <tr>
            <td>";
              if(!empty($_POST['Needs-Gon-support-stand'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "GON SUPPORT STAND</td>
            <td colspan=\"2\">  <span style=\"text-decoration:underline;\">".sterilize($_POST['Size-of-Gon']) ."</span> Size of               Gon</td>
          </tr>
          <tr>
            <td>";
              if(!empty($_POST['Needs-Gon-dumper'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "GONDOLA DUMPER</td>
            <td colspan=\"2\">  <span style=\"text-decoration:underline;\">".sterilize($_POST['max-wt-of-Gon']) ."</span> Max Wt of Gon</td>
          </tr>
          <tr>
            <td colspan=\"3\">";
              if(!empty($_POST['Needs-conveyor'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "CONVEYOR</td>
          </tr>
          <tr>
            <td colspan=\"3\">";
              if(!empty($_POST['Needs-load-place'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "LOAD PLACE</td>
          </tr>
          <tr>
            <td colspan=\"3\">";
              if(!empty($_POST['Needs-inspect-sort'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "INSPECT SORT (*)</td>
          </tr>
          <tr>
            <td>";
              if(!empty($_POST['Needs-weigh-and-dump'])== "yes"){ $body .= "&#9745;"; } else { $body .= "&#9744;"; }
              $body .= "WEIGH AND DUMP:</td>
            <td colspan=\"3\">&nbsp;</td>
          </tr>
          <tr>
            <td> </td>
            <td colspan=\"3\">  <span style=\"text-decoration:underline;\">".sterilize($_POST['Needs-dumps-hour']) ."</span> dumps/hour</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan=\"3\">  <span style=\"text-decoration:underline;\">".sterilize($_POST['Needs-lbs-dump']) ."</span> lbs/dump</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td colspan=\"3\">  <span style=\"text-decoration:underline;\">".sterilize($_POST['Needs-lbs-hour']) ."</span> lbs/hour</td>
          </tr>
        </tbody>
      </table></td>
  </tr>
</table>";

	/* Sends the mail and outputs the "Thank you" string if the mail is successfully sent, or the error string otherwise. */
	if (mail($toEmail,$subject,$body,$h)) {
	  $context["message"] = "<div style='margin-bottom:25px; font-weight:bold'>Thank you for submitting your quote to Spectrum Automation. We will review your quote and contact you soon. </div>";
	} else {
	  $context["message"] = "<b>We apologize but your quote was unable to be submitted. Please try again.</b>";
	}

}

echo $twig->render('requestquote.html', $context);