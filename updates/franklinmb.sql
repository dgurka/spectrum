
-- phpMyAdmin SQL Dump
-- version 2.11.11.3
-- http://www.phpmyadmin.net
--
-- Host: 68.178.216.147
-- Generation Time: Mar 16, 2014 at 06:47 PM
-- Server version: 5.0.96
-- PHP Version: 5.1.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `franklinmb`
--

-- --------------------------------------------------------

--
-- Table structure for table `cache`
--

CREATE TABLE `cache` (
  `key` varchar(255) NOT NULL,
  `value` text,
  PRIMARY KEY  (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cache`
--


-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE `configuration` (
  `key` varchar(255) NOT NULL,
  `value` text,
  PRIMARY KEY  (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `configuration`
--

INSERT INTO `configuration` VALUES('version_id', '3');

-- --------------------------------------------------------

--
-- Table structure for table `directory`
--

CREATE TABLE `directory` (
  `ID` int(11) NOT NULL auto_increment,
  `parent` int(11) NOT NULL,
  `name` varchar(255) default NULL,
  `address` varchar(255) default NULL,
  `address_2` varchar(255) default NULL,
  `city` varchar(255) default NULL,
  `state` varchar(10) default NULL,
  `zip` varchar(10) default NULL,
  `phone` varchar(45) default NULL,
  `fax` varchar(45) default NULL,
  `email` varchar(255) default NULL,
  `contact_person` varchar(255) default NULL,
  `contact_phone` varchar(45) default NULL,
  `website` varchar(255) default NULL,
  `hours` varchar(255) default NULL,
  `img_loc` varchar(255) default NULL,
  `description` text,
  `lat` varchar(45) default NULL,
  `long` varchar(45) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `directory`
--


-- --------------------------------------------------------

--
-- Table structure for table `directory_cat`
--

CREATE TABLE `directory_cat` (
  `ID` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `directory_cat`
--


-- --------------------------------------------------------

--
-- Table structure for table `directory_sub_cat`
--

CREATE TABLE `directory_sub_cat` (
  `ID` int(11) NOT NULL auto_increment,
  `parent` int(11) NOT NULL,
  `name` varchar(255) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `directory_sub_cat`
--


-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `ID` int(11) NOT NULL auto_increment,
  `submitter_name` varchar(255) default NULL,
  `submitter_phone` varchar(45) default NULL,
  `submitter_email` varchar(255) default NULL,
  `event_title` varchar(255) default NULL,
  `caption` varchar(255) default NULL,
  `start_date` date default NULL,
  `start_time` time default NULL,
  `end_date` date default NULL,
  `end_time` time default NULL,
  `type` int(11) default NULL,
  `location` varchar(255) default NULL,
  `location_address` varchar(255) default NULL,
  `description` text,
  `img_loc` varchar(255) default NULL,
  `pending` tinyint(1) default NULL,
  `featured` tinyint(1) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `event`
--


-- --------------------------------------------------------

--
-- Table structure for table `gallery_image`
--

CREATE TABLE `gallery_image` (
  `ID` int(11) NOT NULL auto_increment,
  `gallery` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `caption` varchar(255) default NULL,
  `imgloc` varchar(255) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Dumping data for table `gallery_image`
--

INSERT INTO `gallery_image` VALUES(2, 0, 0, '', 'images/gallery_images/53264c6dd3c35.6100390611_1f218eb6f7.jpg');
INSERT INTO `gallery_image` VALUES(3, 0, 0, '', 'images/gallery_images/53264c7350141.6100392575_a1247fd4d6.jpg');
INSERT INTO `gallery_image` VALUES(4, 0, 0, '', 'images/gallery_images/53264c7906a56.6100393403_9fd61f2425.jpg');
INSERT INTO `gallery_image` VALUES(5, 0, 0, '', 'images/gallery_images/53264c7d649b6.6100936750_71df6fe91e.jpg');
INSERT INTO `gallery_image` VALUES(6, 0, 0, '', 'images/gallery_images/53264c832b562.6100936984_3644486b90.jpg');
INSERT INTO `gallery_image` VALUES(7, 0, 0, '', 'images/gallery_images/53264c922d203.6100937456_eac7583ca2.jpg');
INSERT INTO `gallery_image` VALUES(8, 0, 0, '', 'images/gallery_images/53264c99036df.6100937728_bda323bc78.jpg');
INSERT INTO `gallery_image` VALUES(9, 0, 0, '', 'images/gallery_images/53264ca0191bb.6100938028_9066441f26.jpg');
INSERT INTO `gallery_image` VALUES(10, 0, 0, '', 'images/gallery_images/53264ca6dd17b.6100938260_03d1af45a9.jpg');
INSERT INTO `gallery_image` VALUES(11, 0, 0, '', 'images/gallery_images/53264cb6a52ff.6100938490_2f7b52e908.jpg');
INSERT INTO `gallery_image` VALUES(12, 0, 0, '', 'images/gallery_images/53264cc0a7983.6100938708_b58bf76c56.jpg');
INSERT INTO `gallery_image` VALUES(13, 0, 0, '', 'images/gallery_images/53264ccaaf28e.6100939268_87968a7e43.jpg');
INSERT INTO `gallery_image` VALUES(14, 0, 0, '', 'images/gallery_images/53264cdd05ba6.6100939600_b9ef93603f.jpg');
INSERT INTO `gallery_image` VALUES(15, 0, 0, '', 'images/gallery_images/53264ce2a17dc.6100940068_2a14b2b5e6.jpg');
INSERT INTO `gallery_image` VALUES(17, 1, 0, '', 'images/gallery_images/5326508a94d4e.8177762767_5b1d4ba212.jpg');
INSERT INTO `gallery_image` VALUES(18, 1, 0, '', 'images/gallery_images/53265097eda5b.8177767323_040a0c7fb8.jpg');
INSERT INTO `gallery_image` VALUES(19, 1, 0, '', 'images/gallery_images/5326509d634a2.8177770131_5fea022607.jpg');
INSERT INTO `gallery_image` VALUES(20, 1, 0, '', 'images/gallery_images/532650b62660d.8177772191_be4ac2f4eb.jpg');
INSERT INTO `gallery_image` VALUES(21, 1, 0, '', 'images/gallery_images/532650c6b11d6.8177774183_a54b333cc2.jpg');
INSERT INTO `gallery_image` VALUES(22, 1, 0, '', 'images/gallery_images/532650cd88fe3.8177774397_a280671b84.jpg');
INSERT INTO `gallery_image` VALUES(23, 1, 0, '', 'images/gallery_images/532650d8bd9e8.8177774521_741877106d.jpg');
INSERT INTO `gallery_image` VALUES(24, 1, 0, '', 'images/gallery_images/532650ef9de8a.8177775561_a6dd0f9f29.jpg');
INSERT INTO `gallery_image` VALUES(25, 1, 0, '', 'images/gallery_images/532650f59667d.8177792702_c51d655e86.jpg');
INSERT INTO `gallery_image` VALUES(26, 1, 0, '', 'images/gallery_images/532650ff42d9e.8177793018_a727d36219.jpg');
INSERT INTO `gallery_image` VALUES(27, 1, 0, '', 'images/gallery_images/5326511617e83.8177793300_8d30de3b0f.jpg');
INSERT INTO `gallery_image` VALUES(30, 1, 0, '', 'images/gallery_images/5326513c976dd.8177805032_1e7a96ef17.jpg');
INSERT INTO `gallery_image` VALUES(29, 1, 0, '', 'images/gallery_images/53265127e6de8.8177800472_0e137a0628.jpg');
INSERT INTO `gallery_image` VALUES(31, 1, 0, '', 'images/gallery_images/53265144ae343.8177806480_cb8aa334fe.jpg');
INSERT INTO `gallery_image` VALUES(32, 1, 0, '', 'images/gallery_images/5326514e355ba.8180178985_efb20eb58c.jpg');
INSERT INTO `gallery_image` VALUES(33, 1, 0, '', 'images/gallery_images/5326515465326.8180179269_62239c71a4.jpg');
INSERT INTO `gallery_image` VALUES(34, 1, 0, '', 'images/gallery_images/5326515ee9a92.8180179747_1fcca0f1a1.jpg');
INSERT INTO `gallery_image` VALUES(35, 1, 0, '', 'images/gallery_images/532651660198a.8180215800_ae56b04520.jpg');
INSERT INTO `gallery_image` VALUES(36, 2, 0, '', 'images/gallery_images/5326517a4d3e7.8180172391_071806accc.jpg');
INSERT INTO `gallery_image` VALUES(37, 2, 0, '', 'images/gallery_images/53265184c3ec2.8180173431_e0b88d247d.jpg');
INSERT INTO `gallery_image` VALUES(38, 2, 0, '', 'images/gallery_images/53265188f1dda.8180173793_a5948f4d0a.jpg');
INSERT INTO `gallery_image` VALUES(39, 2, 0, '', 'images/gallery_images/5326519369b8a.8180174479_154dd12269.jpg');
INSERT INTO `gallery_image` VALUES(40, 2, 0, '', 'images/gallery_images/53265198ac90e.8180174813_c89951c28f.jpg');
INSERT INTO `gallery_image` VALUES(41, 2, 0, '', 'images/gallery_images/5326519dcc9e1.8180203566_055fec99dd.jpg');
INSERT INTO `gallery_image` VALUES(42, 2, 0, '', 'images/gallery_images/532651a652aa9.8180205736_a3f5571cea.jpg');
INSERT INTO `gallery_image` VALUES(43, 2, 0, '', 'images/gallery_images/532651ae98026.8180206628_4493728c43.jpg');
INSERT INTO `gallery_image` VALUES(44, 2, 0, '', 'images/gallery_images/532651c28899b.8180207826_de2e7b23f0.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `ID` int(11) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` VALUES(1, 'Home');
INSERT INTO `menu` VALUES(2, 'Photo Galleries');
INSERT INTO `menu` VALUES(3, 'Fundraising');
INSERT INTO `menu` VALUES(4, 'Drumline');

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE `page` (
  `ID` int(11) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `description` text,
  `content` text,
  `menukey` int(11) NOT NULL default '-1',
  `menuorder` int(11) NOT NULL default '0',
  `linkoverride` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT=' ' AUTO_INCREMENT=13 ;

--
-- Dumping data for table `page`
--

INSERT INTO `page` VALUES(1, 'Home', '', '<h2>Kroger Card</h2>\r\n\r\n<p>You can now link your Kroger card to the Franklin Marching Band. When you shop, the band will receive money. For information on how to enroll, or re-enroll, please click here:&nbsp;<a href="http://www.kroger.com/mykroger/018/community/Pages/community_rewards.aspx">http://www.kroger.com/mykroger/018/community/Pages/community_rewards.aspx</a>&nbsp;</p>\r\n\r\n<p>The identification number for the band is 90859.&nbsp;</p>\r\n\r\n<h2>Gordon Food Service</h2>\r\n\r\n<p>If you shop at Gordon Food Service (GFS), make sure to get a Fun Funds card so the bands will benefit every time you shop there. It only takes a minute to get a card. Make sure to tell them that you are with the<strong> Franklin Band Boosters&nbsp;</strong>(not Franklin High School). The band will get a percentage of your purchase.&nbsp;GFS used to credit the Band Boosters even if you did not have a card, but now they require a card.&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', 1, 0, '');
INSERT INTO `page` VALUES(2, 'Fundraising', '', '<h2>EVERGREEN FUNDRAISER</h2>\r\n\r\n<p>- The Evergreen Sales flyer came home October 14. The sale runs until November 1st and proceeds go directly to the students&#39; accounts. Items will be delivered November 25<sup>th. </sup>Please support the band and ask your family and friends for their orders. Any questions contact Shelly Robinet at shellrobinet@gmail.com.</p>\r\n\r\n<h2><br />\r\nPALACE SPORTS&amp;ENTERTAINMENT CONCESSIONS</h2>\r\n\r\n<p>We have an opportunity to earn money for the band and/or individual band accounts by partnering with Volunteers for Education (VE); a non-profit group that works concession stands at the Palace<br />\r\nof Auburn Hills and DTE Energy Music Theatre.&nbsp; Anyone over the age of 18<br />\r\ncan sign-up as an individual or as a group. You make a minimum of about $35 or<br />\r\nmuch more if it is a particularly successful event.&nbsp; There are openings to<br />\r\nwork Piston games through April, and we will be made aware by VE of concerts as<br />\r\nthey become available. Please think of this as a great opportunity to raise<br />\r\nmoney for your student and/or the band, while working with some great people<br />\r\nand experiencing a fun event! If you are interested in signing up for an event<br />\r\nor learning more about the program, please contact Jill Jackson from Volunteers<br />\r\nfor Education at <a href="mailto:jillifris@sbcglobal.net">jillifris@sbcglobal.net</a> or Karen Gardner at <a href="mailto:karengardner@sbcglobal.net">karengardner@sbcglobal.net</a>.</p>\r\n\r\n<p>&nbsp;</p>\r\n', 3, 0, '');
INSERT INTO `page` VALUES(3, 'Drumline', '', '<p>Go to <a href="http://www.motorcitypercussion.com" title="www.motorcitypercussion.com">www.motorcitypercussion.com</a> for more info.</p>\r\n', 4, 0, '');
INSERT INTO `page` VALUES(4, '2011 Band Camp', '', '<p>text</p>\r\n', 2, 8, './gallery/0/');
INSERT INTO `page` VALUES(5, '2012 Band Camp - Practice', '', '', 2, 7, './gallery/1/');
INSERT INTO `page` VALUES(6, 'Placeholder', '', '', 2, 9, '#');
INSERT INTO `page` VALUES(7, '2012 Band Camp - The Band', '', '', 2, 6, './gallery/2/');
INSERT INTO `page` VALUES(8, '2012 Band Camp - Fun', '', '', 2, 5, './gallery/3/');
INSERT INTO `page` VALUES(9, '2012 Band Camp - Seniors', '', '', 2, 4, './gallery/4/');
INSERT INTO `page` VALUES(10, 'John Glenn Football 9-16-11', '', '', 2, 3, './gallery/5/');
INSERT INTO `page` VALUES(11, 'Pepsi Refresh Challenge', '', '', 2, 2, './gallery/6/');
INSERT INTO `page` VALUES(12, 'Plymouth Football 10-14-11', '', '', 2, 1, './gallery/7/');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `ID` int(11) NOT NULL auto_increment,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY  (`ID`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `user`
--

